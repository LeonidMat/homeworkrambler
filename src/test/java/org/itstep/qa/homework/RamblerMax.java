package org.itstep.qa.homework;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RamblerMax {
    private WebDriver driver;

    @BeforeClass
    public void createBrowser() {
        System.setProperty("webdriver.chrome.driver",
                "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void quitBrowser() {
    }

    @BeforeMethod
    public void clear() {
        driver.manage().deleteAllCookies();
    }

    @Test
    public void registration() {
        driver.get("https://id.rambler.ru/account/registration");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"firstname\"]"));
        element.sendKeys("Ivan");
        element = driver.findElement(By.xpath("//*[@id=\"lastname\"]"));
        element.sendKeys("Ivanov");
        element = driver.findElement(By.xpath("//*[@id=\"login\"]"));
        element.sendKeys("IvanIvanovIvanovichPetrovichaDoch"
                + Keys.ENTER);
        element = driver.findElement(By.cssSelector("#root > div > div.src-components-UnauthPage-styles--main--3FZQp > form > section:nth-child(3) > div > div > div.rui-InputStatus-message"));
        Assert.assertEquals(element.getText(),"Логин должен быть от 3 до 32 символов");
    }
}


