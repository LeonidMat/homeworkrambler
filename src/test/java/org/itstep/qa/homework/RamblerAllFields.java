package org.itstep.qa.homework;/*Написать тест на проверку заполнения всех полей
(проверять что отображаются соответствующие всплывающие ошибки
с соответствующим текстом)*/


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RamblerAllFields {
    private WebDriver driver;

    @BeforeClass
    public void createBrowser() {
        System.setProperty("webdriver.chrome.driver",
                "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void quitBrowser() {
    }

    @BeforeMethod
    public void clear() {
        driver.manage().deleteAllCookies();
    }

    @Test
    public void registration() {
        driver.get("https://id.rambler.ru/account/registration");
        WebElement element = driver.findElement(By.xpath("//*[@id=\"firstname\"]"));
        element.sendKeys(""+Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[1]/div/div/div[2]"));
        Assert.assertEquals(element.getText(),"Поле должно быть заполнено");
        element = driver.findElement(By.xpath("//*[@id=\"lastname\"]"));
        element.sendKeys(""+Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[2]/div/div/div[2]"));
        Assert.assertEquals(element.getText(),"Поле должно быть заполнено");
        element = driver.findElement(By.xpath("//*[@id=\"login\"]"));
        element.sendKeys(""+Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[3]/div/div/div[3]"));
        Assert.assertEquals(element.getText(),"Логин должен быть от 3 до 32 символов");
        element = driver.findElement(By.xpath("//*[@id=\"newPassword\"]"));
        element.sendKeys(""+Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[4]/div/div[1]/div[2]"));
        Assert.assertEquals(element.getText(),"Пароль должен содержать от 8 до 32 символов, включать " +
                "хотя бы одну заглавную латинскую букву, одну строчную и одну цифру");
        element = driver.findElement(By.xpath("//*[@id=\"birthday\"]"));
        element.sendKeys(Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[6]/div/div/div[1]/div[2]/div/div/div/input"));
        element.sendKeys(Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[6]/div/div/div[1]/div[3]/div/div/div/input"));
        element.sendKeys(Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[6]/div/div/div[2]"));
        Assert.assertEquals(element.getText(),"Укажите дату рождения полностью");

        element = driver.findElement(By.xpath("//*[@id=\"gender\"]"));
        element.sendKeys(Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[7]/div/div/div[2]"));
        Assert.assertEquals(element.getText(),"Выберите пол");

        element = driver.findElement(By.xpath("//*[@id=\"geoid\"]"));
        element.sendKeys(Keys.ENTER);

        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/div/div/div/section/div/div[1]/div[1]/div[1]/div/div/div/input"));
        element.sendKeys(Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/div/div/div/section/div/div[1]/div[1]/div[1]/div/div/div/input"));
        element.sendKeys(Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/div/div/div/section/div/div[1]/div[2]"));
        Assert.assertEquals(element.getText(),"Введите телефон");
    }
}


